#include "hwinit/util.h"
#include "hwinit/uart.h"
#include "hwinit/timer.h"
#include "hwinit/pmc.h"
#include "hwinit/t210.h"
#include "hwinit/i2c.h"
#include "lib/printk.h"
#include "dumpcode.h"
#include "cmd.h"
#include <stdio.h>
#include <string.h>

static void cmd_help() {
    printk("%s", "MemXPlorer by perillamint - Licensed under GPLv2\n\n"
           "help - print this help message.\n"
           "shutdown - shutdown system\n"
           "reboot - reboot system into RCM\n"
           "powercycle - power cycle system using BQ24193 shipping mode.\n"
           "peek [hex addr] [dec size] - dump values at given address.\n"
           "poke [hex addr] [hex value] - set memory at given address with given value.\n");
    printk("i2cpeek [bus #] [hex i2c addr] [hex reg addr] [dec size] - dump values at given i2c dev.\n"
           "max17050_dump - dump max17050 regs\n"
           "max17050_salrt_off - turn off MAX17050 S_ALRT\n"
           "bq24193_dump - dump bq24193 regs\n"
           "fix_cnfgglbl1 - Try to fix 'battery desync' bug by restoring MAX77620_REG_CNFGGLBL1\n"
	   "                reg value to 0x92\n");

           printk("\nWARN!! WARN!! WARN!!\n"
           "peek and poke can cause undesired behavior.\n"
           "powercycle command will shut down and reset system RTC and PMC\n"
           "Use it with care.\n\n");
}

static void cmd_shutdown() {
    printk("Shutting down full system.\n"
           "Have a nice day!\n");
    sleep(10000);
    shutdown_using_pmic();
}

static void cmd_reboot() {
    printk("Rebooting into RCM.\n");
    sleep(10000);
    PMC(APBDEV_PMC_SCRATCH0) = 2; // Reboot into rcm.
    PMC(0) |= 0x10;
    while (1)
        sleep(1);
}

static int cmd_powercycle() {
    // Code from https://github.com/crystalseedgba/BatteryFix/blob/master/fusee/src/main.c
    printk("Checking BQ24193...\n");
    const int kI2cBus = 0;
    const int kBq24193SlaveAddress = 0x6b;

    uint8_t part_info;
    i2c_recv_buf_small(&part_info, sizeof(part_info),
                       kI2cBus, kBq24193SlaveAddress, 0x0a);
    if (part_info != 0x2f) {
        printk("ERROR: charger part number mismatch got: "
               "0x%02x want: 0x%02x\n\n   ", part_info, 0x2f);
        return 1;
    }

    printk("BQ24193 OK!\n");
    printk("Entering BQ24193 shipping mode.\n");
    printk("Disabling BQ24193 Watchdog...\n");

    // Disable watchdog.
    uint8_t reg5;
    i2c_recv_buf_small(&reg5, sizeof(reg5), kI2cBus,
                       kBq24193SlaveAddress, 0x05);
    reg5 &= ~((1 << 4 | 1 << 5));
    i2c_send_buf_small(kI2cBus, kBq24193SlaveAddress,
                       0x05, &reg5, sizeof(reg5));

    printk("Shutting BATFET down...\n");
    // Disable BATFET.
    uint8_t reg7;
    i2c_recv_buf_small(&reg7, sizeof(reg7), kI2cBus,
                       kBq24193SlaveAddress, 0x07);
    reg7 |= (1 << 5);
    i2c_send_buf_small(kI2cBus, kBq24193SlaveAddress, 0x07,
                       &reg7, sizeof(reg7));
    printk("BATFET shutdown complete.\n");
    printk("Unplug USB cable and replug it to complete power cycle procedure\n");
    return 0;
}

static int cmd_peek(const char *buf) {
    int ret;
    void *addr;
    int size;
    char cmdbuf[3][512];

    ret = sscanf(buf, "%s %s %s", cmdbuf[0], cmdbuf[1], cmdbuf[2]);
    if(ret < 0)
    {
        printk("Error while parsing command.\n");
        printk("Use help command to get help.\n");
        return -1;
    }

    if(strncmp(cmdbuf[1], "0x", 2) != 0)
    {
        printk("Address must starts with 0x.\n");
        return -1;
    }

    ret = sscanf(cmdbuf[1] + 2, "%x", (unsigned int*)&addr);
    if(ret < 0)
    {
        printk("Invalid address format.\n");
        return -1;
    }

    ret = sscanf(cmdbuf[2], "%d", &size);
    if(ret < 0)
    {
        printk("Invalid size format.\n");
        return -1;
    }

    dumpcode(addr, size);
    return 0;
}

static int cmd_poke(const char *buf) {
    int ret;
    void *addr;
    int size;
    char cmdbuf[3][512];

    union omnidata_u {
        uint32_t uint32;
        uint16_t uint16;
        uint8_t uint8;
    } omnidata;

    ret = sscanf(buf, "%s %s %s", cmdbuf[0], cmdbuf[1], cmdbuf[2]);
    if(ret < 0)
    {
        printk("Error while parsing command.\n");
        printk("Use help command to get help.\n");
        return -1;
    }

    if(strncmp(cmdbuf[1], "0x", 2) != 0)
    {
        printk("Address must starts with 0x.\n");
        return -1;
    }

    ret = sscanf(cmdbuf[1] + 2, "%x", (unsigned int*)&addr);
    if(ret < 0)
    {
        printk("Invalid address format.\n");
        return -1;
    }

    if(strncmp(cmdbuf[2], "0x", 2) != 0)
    {
        printk("Data value must starts with 0x.\n");
        return -1;
    }

    ret = sscanf(cmdbuf[2] + 2, "%lx", &omnidata.uint32);
    if(ret < 0)
    {
        printk("Invalid address format.\n");
        return -1;
    }

    size = strlen(cmdbuf[2] + 2);

    if(size <= 2)
    {
        *((uint8_t*)addr) = omnidata.uint8;
    }
    else if(size <= 4)
    {
        *((uint16_t*)addr) = omnidata.uint16;
    }
    else if(size <= 8)
    {
        *((uint32_t*)addr) = omnidata.uint32;
    }
    else
    {
        printk("Invalid value length.\n");
        return -1;
    }

    return 0;
}

static int cmd_i2cpeek(const char *buf) {
    int ret;
    int bus;
    int addr;
    int reg;
    int size;
    u8 i2cbuf[512];
    char cmdbuf[5][512];

    ret = sscanf(buf, "%s %s %s %s %s", cmdbuf[0], cmdbuf[1], cmdbuf[2], cmdbuf[3], cmdbuf[4]);
    if(ret < 0) {
        printk("Error while parsing command.\n");
        printk("Use help command to get help.\n");
        return -1;
    }

    ret = sscanf(cmdbuf[1], "%d", &bus);
    if(ret < 0) {
        printk("Invalid bus number.\n");
        return -1;
    }

    if(strncmp(cmdbuf[2], "0x", 2) != 0) {
        printk("Address must starts with 0x.\n");
        return -1;
    }

    ret = sscanf(cmdbuf[2] + 2, "%x", &addr);
    if(ret < 0) {
        printk("Invalid address format.\n");
        return -1;
    }

    if(strncmp(cmdbuf[3], "0x", 2) != 0) {
        printk("I2C reg address must starts with 0x.\n");
        return -1;
    }

    ret = sscanf(cmdbuf[3] + 2, "%x", &reg);
    if(ret < 0) {
        printk("Invalid I2C reg address format.\n");
        return -1;
    }

    ret = sscanf(cmdbuf[4], "%d", &size);
    if(ret < 0) {
        printk("Invalid size format.\n");
        return -1;
    }

    ret = i2c_recv_buf_small(i2cbuf, size, bus, addr, reg);
    if (ret != 1) {
        printk("WARN: I2C recv error!\n");
    }
    dumpcode(i2cbuf, size);
    return 0;
}

const int cmd_max17050_dump() {
    u16 i2cbuf;
    const int kI2cBus = 0;
    const int kMax17050Address = 0x36;

    for(int i = 0; i <= 0xff; i++) {
        switch (i) {
        case 0x0c:
        case 0x14:
        case 0x15:
        case 0x20:
        case 0x26:
        case 0x30:
        case 0x31:
        case 0x33:
        case 0x34:
        case 0x35:
        case 0x3b:
        case 0x3c:
        case 0x40:
        case 0x41:
        case 0x43:
        case 0x44:
            //printk("0x%02x = RESERVED\n", i);
            break;
        default:
            if ((0x47 <= i && 0x4c >= i) ||
                (0x4e <= i && 0x7f >= i) ||
                (0xb0 <= i && 0xfa >= i) ||
                (0xfc <= i && 0xfe >= i)) {
                //printk("0x%02x = RESERVED\n", i);
                break;
            }

            i2c_recv_buf_small((u8*)&i2cbuf, 2, kI2cBus, kMax17050Address, i);
            printk("0x%02x = 0x%04x\n", i, i2cbuf);
        }
    }

    return 0;
}

const int cmd_max17050_salrt_off() {
    const int kI2cBus = 0;
    const int kMax17050Address = 0x36;\
    const u16 offpayload = 0xff00;
    return i2c_send_buf_small(kI2cBus, kMax17050Address, 0x03, (u8*)&offpayload, 2);
}

int read_cmd(char *buf, const int bufsz) {
    int cursor = 0;
    printk("# ");
    for (;;) {
        char tmp = uart_recv(DEBUG_UART_PORT);
        if (0x0D == tmp) {
            printk("\n");
        } else if (0x08 == tmp) {
            if (0 < cursor) {
                printk("%c", 0x08);
                printk(" ");
                printk("%c", 0x08);
            }
        } else {
            printk("%c", tmp);
        }

        if (bufsz > cursor && 0x0D == tmp) {
            buf[cursor] = 0x00;
            return 0;
        } else if (0x0D == tmp) {
            buf[cursor] = 0x00;
            return -1;
        }

        if (0x08 == tmp && 0 < cursor) {
            cursor --;
        } else if (0x08 != tmp) {
            buf[cursor] = tmp;
            cursor++;
        }
    }
}

const int cmd_bq24193_dump() {
    u8 i2cbuf;
    const int kI2cBus = 0;
    const int kBq24193SlaveAddress = 0x6b;

    for(int i = 0; i <= 0x0a; i++) {
            i2c_recv_buf_small(&i2cbuf, 1, kI2cBus, kBq24193SlaveAddress, i);
            printk("0x%02x = 0x%02x\n", i, i2cbuf);
    }

    return 0;
}

const void cmd_fix_cnfgglbl1() {
    u8 data = 0x92;

    printk("Writing 0x92 to MAX77620@0x00\n");
    i2c_send_buf_small(4, 0x3c,
                       0x00, &data, sizeof(data));
}

int eval_cmd(char *buf) {
    const int maxcmdsz = 512;
    char cmd[512] = {0, };
    sscanf(buf, "%s", cmd);

    if (strncmp(cmd, "help", maxcmdsz) == 0) {
        cmd_help();
    } else if (strncmp(cmd, "shutdown", maxcmdsz) == 0) {
        cmd_shutdown();
    } else if (strncmp(cmd, "reboot", maxcmdsz) == 0) {
        cmd_reboot();
    } else if (strncmp(cmd, "powercycle", maxcmdsz) == 0) {
        cmd_powercycle();
    } else if (strncmp(cmd, "peek", maxcmdsz) == 0) {
        cmd_peek(buf);
    } else if (strncmp(cmd, "poke", maxcmdsz) == 0) {
        cmd_poke(buf);
    } else if (strncmp(cmd, "i2cpeek", maxcmdsz) == 0) {
        cmd_i2cpeek(buf);
    } else if (strncmp(cmd, "max17050_dump", maxcmdsz) == 0) {
        cmd_max17050_dump();
    } else if (strncmp(cmd, "max17050_salrt_off", maxcmdsz) == 0) {
        cmd_max17050_salrt_off();
    } else if (strncmp(cmd, "bq24193_dump", maxcmdsz) == 0) {
        cmd_bq24193_dump();
    } else if (strncmp(cmd, "fix_cnfgglbl1", maxcmdsz) == 0) {
        cmd_fix_cnfgglbl1();
    } else {
        printk("Unknown command: %s\n", cmd);
    }

    return 0;
}
