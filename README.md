# MemXPlorer ![License](https://img.shields.io/badge/License-GPLv2-blue.svg)
Nintendo Switch memory explorer

Lots of codebase are came from https://github.com/rajkosto/memloader 

## Usage
 1. Install DevkitPRO toolchain
 2. Run make
 3. Attach UART JoyCon slide on right side and boot into RCM
 4. Connect to UART console (115200 8n1)
 5. Load build result using fusee gelee exploit
 6. Type help and enjoy :)

## Changes
See commit history ;)

## Responsibility

**I am not responsible for anything, including dead switches, loss of life, or total nuclear annihilation.**
